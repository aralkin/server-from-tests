const express = require('express');
const app = express();
const PORT = 7070;


const OPERATION_NAME = {
  SUMMATION: 'summation',
  MULTIPLICATION: 'multiplication',
};


const operations = {
  summation: 0,
  multiplication: 0,
  total: 0,
  lastOperation: null,
};

const addToOperations = (operationName) => {
  operations.total += 1;
  operations.lastOperation = operationName;
  if (operationName === 'multiplication') operations.multiplication += 1;
  if (operationName === 'summation') operations.summation += 1;
}


const history = [];

const addToHistory = (result, operationName) => {
  const newOperation = {
    operationName,
    ...result,
    createdAt: new Date()
  };
  history.push(newOperation);
}


const getDurationInMilliseconds  = (start) => {
  const NS_PER_SEC = 1e9
  const NS_TO_MS = 1e6
  const diff = process.hrtime(start)
  return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS
}


app.get('/information', (req, res) => {
  res.status(200)
  res.send({
    version: '0.0.1',
    uptime: Math.floor(process.uptime()),
    timeOfResponse: getDurationInMilliseconds(req.time)
  });
});

app.get(`/operation/${OPERATION_NAME.SUMMATION}`, (req, res) => {
  const firstOperand = Number(req.query.firstOperand);
  const secondOperand = Number(req.query.secondOperand)
  res.status(200)

  const result = {
    firstOperand: firstOperand,
    secondOperand: secondOperand,
    total: firstOperand + secondOperand,
    timeOfResponse: getDurationInMilliseconds(req.time)
  }
  res.send(result);
  addToOperations(OPERATION_NAME.SUMMATION)
  addToHistory(result, OPERATION_NAME.SUMMATION)

});

app.get('/operation/last', (req, res) => {
  res.status(200)
  res.send({
    lastOperationName: operations.lastOperation,
    timeOfResponse: getDurationInMilliseconds(req.time)
  });
});


app.get(`/operation/${OPERATION_NAME.MULTIPLICATION}`, (req, res) => {
  const firstMultOperand = Number(req.query.firstOperand);
  const secondMultOperand = Number(req.query.secondOperand)
  res.status(200)
  const result = {
    firstOperand: firstMultOperand,
    secondOperand: secondMultOperand,
    total: firstMultOperand * secondMultOperand,
    timeOfResponse: getDurationInMilliseconds(req.time)
  }
  res.send(result);
  addToOperations(OPERATION_NAME.MULTIPLICATION)
  addToHistory(result, OPERATION_NAME.MULTIPLICATION);
});


app.get('/operation/information', (req, res) => {
  res.status(200)
  res.send({
    totalAmountOfOperation: operations.total,
    summation: operations.summation,
    multiplication: operations.multiplication,
    timeOfResponse: getDurationInMilliseconds(req.time)
  });
});


app.get('/operation/history', (req, res) => {
  const { sort } = req.query
  if (sort === 'DESC') {
    const historySortedByDesc = history.slice(0).sort((a, b) => b.createdAt - a.createdAt)
    const datelessHistorySortedByDesc = historySortedByDesc.map(object => {
      const { createdAt, ...result } = object
      return result;
    })
    res.status(200)
    res.send({
      history: datelessHistorySortedByDesc,
      timeOfResponse: getDurationInMilliseconds(req.time)
    });
  }
  else if (sort === 'ASC') {
    const historySortedByAsc = history.slice(0).sort((a, b) => a.createdAt - b.createdAt)
    const datelessHistorySortedByAsc = historySortedByAsc.map(object => {
      const { createdAt, ...result } = object
      return result;
    })
    res.status(200)
    res.send({
      history: datelessHistorySortedByAsc,
      timeOfResponse: getDurationInMilliseconds(req.time)
    });
  }
});


app.listen(PORT, () => {
  console.log(`Server running at: http://localhost:${PORT}/`);
});

module.exports = app;